package com.pot;

import com.pot.xmlway.Forest;
import com.pot.xmlway.Hunter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class SpringPracticeApplication {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("file:src/main/java/com/pot/config.xml");

        Forest forest = (Forest) context.getBean("forest");
        System.out.println(forest);

        Hunter hunter = (Hunter) context.getBean("bowHunter");
        hunter.hunt();

        SpringApplication.run(SpringPracticeApplication.class, args);
    }
}
