package com.pot.xmlway;

/**
 * Created by priv on 07.12.15.
 */
public class Forest {

    int area = 100;

    private Forest() {
    }

    private static  class ForestSingletonHolder {
        static Forest instance = new Forest();
    }

    public static Forest getInstance() {
        return ForestSingletonHolder.instance;
    }

    @Override
    public String toString() {
        return "Forest area " + this.area;
    }
}
