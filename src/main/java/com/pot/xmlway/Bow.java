package com.pot.xmlway;

/**
 * Created by sokoloj1 on 2015-11-19.
 */
public class Bow implements Weapon {

    String name;
    int attackPoints;

    public Bow(String name, int attackPoints) {
        this.name = name;
        this.attackPoints = attackPoints;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int attack() {
        return this.attackPoints;
    }

}
